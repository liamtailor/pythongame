from pygamii.objects import Object
import src.Constants.curserColors as color


def initialize_outer_walls(scene):
    left_wall = Wall(0, 0, 1, 100)
    right_wall = Wall(80, 0, 1, 100)
    top_wall = Wall(0, 0, 100, 1)
    bottom_wall = Wall(0, 22, 100, 1)

    scene.add_object(left_wall)
    scene.add_object(right_wall)
    scene.add_object(top_wall)
    scene.add_object(bottom_wall)


class Wall(Object):

    def __init__(self, x, y, width, height):
        self.width = width
        self.height = height
        self.x = x
        self.y = y
        self.char = '-' if width > height else '|'

    x = 0
    y = 0
    width = 100
    height = 1
    color = color.YELLOW
    char = '-'

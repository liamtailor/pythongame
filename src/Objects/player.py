from pygamii.objects import Object
import src.Constants.curserColors as color
from src.Objects.walls import Wall


class Player(Object):
    width = 1
    height = 1
    x = 3
    y = 3
    previous_x = x
    previous_y = y
    color = color.BLUE
    scene_objects = []

    def __init__(self, scene_objects):
        self.scene_objects = scene_objects

    def __str__(self):
        return '@'

    def up(self):
        self.save_previous_position()
        self.y -= 1
        self.check_collisions()

    def down(self):
        self.save_previous_position()
        self.y += 1
        self.check_collisions()

    def left(self):
        self.save_previous_position()
        self.x -= 1
        self.check_collisions()

    def right(self):
        self.save_previous_position()
        self.x += 1
        self.check_collisions()

    def save_previous_position(self):
        self.previous_x = self.x
        self.previous_y = self.y

    def check_collisions(self):
        for obj in self.scene_objects:
            if isinstance(obj, Wall) and self.collision(obj):
                self.x = self.previous_x
                self.y = self.previous_y

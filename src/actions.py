from pygamii.action import BaseKeyboard
import src.Constants.keyboardMap as keyboardMap


class Keyboard(BaseKeyboard):
    def handler(self, key):
        if key != -1:
            print()

        if key == keyboardMap.UP:
            self.scene.player.up()
        elif key == keyboardMap.DOWN:
            self.scene.player.down()
        elif key == keyboardMap.LEFT:
            self.scene.player.left()
        elif key == keyboardMap.RIGHT:
            self.scene.player.right()
        elif key == keyboardMap.EXIT:
            self.scene.stop()

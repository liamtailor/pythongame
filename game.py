from pygamii.scene import BaseScene
from src.Objects.player import Player
import src.Objects.walls as walls
from src.actions import Keyboard


class Scene(BaseScene):
    def __init__(self, *args, **kwargs):
        super(Scene, self).__init__(*args, **kwargs)
        self.player = Player(self.objects)

        self.add_action(Keyboard())

        walls.initialize_outer_walls(self)

        self.collider = walls.Wall(10, 10, 1, 1)
        self.add_object(self.collider)

        # player added last so he is always on top
        self.add_object(self.player)


if __name__ == '__main__':
    scene = Scene()
    scene.start()
